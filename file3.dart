class Name {
  var name;
  var category;
  var developer;
  var year;

  showApp() {
    print("The app name to Upper case is : ${name.toUpperCase()}");
  }

  appInfo() {
    print("App of the year is : ${name}");
    print("sector/category is : ${category}");
    print("Developer is : ${developer}");
    print("Year is : ${year}");
  }
}

void main() {
  var app = new Name();
  app.name = "application ";
  app.category = "category";
  app.developer = "names";
  app.year = "2000";

  app.appInfo();

  app.showApp();
}
